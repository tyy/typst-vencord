/*
 * Vencord, a modification for Discord's desktop app
 * Copyright (c) 2023 Vendicated and contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { ApplicationCommandInputType, ApplicationCommandOptionType } from "@api/Commands";
import { Settings } from "@api/settings";
import { Devs } from "@utils/constants";
import definePlugin, { OptionType } from "@utils/types";
import { findByCodeLazy } from "@webpack";

const promptToUpload = findByCodeLazy("UPLOAD_FILE_LIMIT_ERROR");
const DRAFT_TYPE = 0;

export default definePlugin({
    name: "Typst",
    description: "A plugin to render typst math code in discord",
    authors: [
        Devs.Tyman
    ],
    dependencies: ["CommandsAPI"],
    options: {
        api: {
            type: OptionType.STRING,
            description: "The API URL to use for rendering math",
            default: "https://typst.tyman.systems/"
        },
        theme: {
            type: OptionType.SELECT,
            options: [
                {
                    label: "Light",
                    value: "light",
                    default: true
                },
                {
                    label: "Dark",
                    value: "dark",
                    default: false
                }
            ],
            description: "The theme to render the image in"
        }
    },
    commands: [{
        name: "math",
        description: "Render math using typst",
        async execute(args, ctx) {
            const img = await fetch(`${Settings.plugins.Typst.api}?${new URLSearchParams({ code: args[0].value, theme: args[1]?.value ?? Settings.plugins.Typst.theme })}`).then(r => r.blob());
            const file = new File([img], "math.png", { type: "image/png" });
            // Immediately after the command finishes, Discord clears all input, including pending attachments.
            // Thus, setTimeout is needed to make this execute after Discord cleared the input
            setTimeout(() => promptToUpload([file], ctx.channel, DRAFT_TYPE), 50);
        },
        options: [
            {
                name: "code",
                description: "The typst math code to render",
                type: ApplicationCommandOptionType.STRING,
                required: true
            },
            {
                name: "theme",
                description: "The theme to render the image in",
                type: ApplicationCommandOptionType.STRING,
                choices: [
                    {
                        label: "Light",
                        name: "Light",
                        value: "light"
                    },
                    {
                        label: "Dark",
                        name: "Dark",
                        value: "dark"
                    }
                ],
                required: false
            }
        ],
        inputType: ApplicationCommandInputType.BUILT_IN
    }]
});
